package urma.practice;

import java.util.Arrays;
import java.util.List;

public class Movie {

    private final String name;
    private final int star;
    private final Rating rating;
    private final boolean playing ;

    public Movie(String name,boolean playing, int star, Rating rating ) {
        this.name = name;
        this.playing = playing;
        this.star = star;
        this.rating = rating;
    }


    public Rating getRating() {
        return rating;
    }

    public boolean isPlaying() {
        return playing;
    }

    public String getName() {
        return name;
    }


    public int getStar() {
        return star;
    }



    public enum Rating { G, PG, PG13, R}

    @Override
    public String toString() {
        return name;
    }


    public static final List<Movie> movies =
            Arrays.asList( new Movie("pixels", false, 91, Movie.Rating.G),
                           new Movie("10 cent pistol", false, 82, Movie.Rating.R),
                           new Movie("paper towns", false, 99, Movie.Rating.R),
                           new Movie("ant man", true, 5, Rating.PG13),
                           new Movie("trainwreck", true, 62, Movie.Rating.PG13),
                           new Movie("inside out", true, 81, Movie.Rating.G),
                           new Movie("the gallows", true, 32, Movie.Rating.PG),
                           new Movie("minious", false, 56, Movie.Rating.PG),
                           new Movie("jurasic world", false, 45, Movie.Rating.R));
}