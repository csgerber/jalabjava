package horstcore.ch03.sec04;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public class LambdaDemo {
    public static void main(String[] args) {
        String[] friends = { "Alexander", "Peter", "X", "Paul", "Mary"  };
//        Arrays.sort(friends,
//                (first, second) -> first.length() - second.length());


        Arrays.sort(friends, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                if (o1.length() - o2.length() >= 1){
                    return 1;
                } else if (o1.length() - o2.length() <= 1){
                    return -1;
                } else {
                    return 0;
                }
            }
        });


        System.out.println(Arrays.toString(friends));
        ArrayList<String> enemies = new ArrayList<>(Arrays.asList("Malfoy", "Crabbe", "Goyle", null));
        enemies.removeIf(e -> e == null);
        System.out.println(enemies);        
    }
}
