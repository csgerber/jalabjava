package urma.chap2;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

/**
 * Created by Adam on 7/14/2015.
 */
public class ConsumerDriver {
    public static void main(String[] args) {

        List<Integer> intSeq = Arrays.asList(1, 2, 3);
        Consumer<Integer> cnsmr = x -> System.out.println(x);
        intSeq.forEach(cnsmr);

    }


}
